package main

import (
	"PonGo/model"
	"strconv"

	rl "github.com/gen2brain/raylib-go/raylib"
)

const screenWidth = 1280
const screenHeight = 800
const paddleOffset = 10

var player_score, computer_score = 0, 0

var ball = model.Ball{
	X:       screenWidth / 2,
	Y:       screenHeight / 2,
	Radius:  20,
	Speed_x: 7,
	Speed_y: 7,
}

var player = model.Paddle{
	Height: 120,
	Width:  25,
	Speed:  6,
}
var computer = model.Computer{
	Paddle: model.Paddle{
		X:      0 + paddleOffset,
		Height: 120,
		Width:  25,
		Speed:  6,
	},
}

func init() {
	rl.InitWindow(screenWidth, screenHeight, "PonGo")
	rl.SetTargetFPS(60)
	player.X = screenWidth - paddleOffset - player.Width
	player.Y = screenHeight/2 - player.Height/2
	computer.Y = screenHeight/2 - computer.Height/2

}

func main() {
	// Game loop
	for !rl.WindowShouldClose() {
		rl.BeginDrawing()
		rl.ClearBackground(rl.Black)

		// Draw
		ball.Draw()
		player.Draw()
		computer.Draw()

		rl.DrawLine(screenWidth/2, 0, screenWidth/2, screenHeight, rl.White)
		rl.DrawText(strconv.Itoa(computer_score), screenWidth/4, 20, 80, rl.White)
		rl.DrawText(strconv.Itoa(player_score), 3*screenWidth/4, 20, 80, rl.White)

		if rl.CheckCollisionCircleRec(rl.NewVector2(ball.X, ball.Y), ball.Radius, player.CollisionRec()) ||
			rl.CheckCollisionCircleRec(rl.NewVector2(ball.X, ball.Y), ball.Radius, computer.CollisionRec()) {
			ball.Speed_x *= -1
		}

		// Updating
		ball.Update(&computer_score, &player_score)
		player.Update()
		computer.Update(ball.Y)
		rl.EndDrawing()
	}

	rl.CloseWindow()
}
