package model

import (
	rl "github.com/gen2brain/raylib-go/raylib"
)

type Paddle struct {
	X      int32
	Y      int32
	Height int32
	Width  int32
	Speed  int32
}

func (p *Paddle) Draw() {
	rl.DrawRectangle(p.X, p.Y, p.Width, p.Height, rl.White)
}

func (p *Paddle) limitMove() {
	if p.Y <= 0 {
		p.Y = 0
	} else if p.Y+p.Height >= int32(rl.GetScreenHeight()) {
		p.Y = int32(rl.GetScreenHeight()) - p.Height
	}
}

func (p *Paddle) Update() {
	if rl.IsKeyDown(rl.KeyUp) {
		p.Y -= p.Speed
	}
	if rl.IsKeyDown(rl.KeyDown) {
		p.Y += p.Speed
	}
	p.limitMove()
}

func (p *Paddle) CollisionRec() rl.Rectangle {
	return rl.NewRectangle(
		float32(p.X),
		float32(p.Y),
		float32(p.Width),
		float32(p.Height),
	)
}
