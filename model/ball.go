package model

import (
	rl "github.com/gen2brain/raylib-go/raylib"
)

type Ball struct {
	X       float32
	Y       float32
	Radius  float32
	Speed_x float32
	Speed_y float32
}

func (b *Ball) Draw() {
	rl.DrawCircle(int32(b.X), int32(b.Y), b.Radius, rl.White)
}

func (b *Ball) Update(compuetr_score, player_score *int) {
	b.X += b.Speed_x
	b.Y += b.Speed_y

	if (b.Y+b.Radius) >= float32(rl.GetScreenHeight()) || (b.Y-b.Radius) <= 0 {
		b.Speed_y *= -1
	}
	if (b.X + b.Radius) >= float32(rl.GetScreenWidth()) {
		*compuetr_score++
		b.Speed_x *= -1
	} else if (b.X - b.Radius) <= 0 {
		*player_score++
		b.Speed_x *= -1
	}
}
