package model

type Computer struct {
	Paddle
}

func (c *Computer) Update(ball_y float32) {
	if c.Y+c.Height/2 > int32(ball_y) {
		c.Y -= c.Speed
	} else {
		c.Y += c.Speed
	}
	c.limitMove()
}
